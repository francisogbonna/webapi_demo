﻿using Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    
    [ApiController]
    [Route("api/customers")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepositoryManager _repositoryManager;
        private readonly ILoggerManager _logger;
        public CustomersController(IRepositoryManager repository, ILoggerManager logger)
        {
            _repositoryManager = repository;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetCustomers()
        {
            try
            {
                var Customers = _repositoryManager.Customer.GetCustomers(trackChanges: false);
                return Ok(Customers);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong in the {nameof(GetCustomers)} action {ex}");
                return StatusCode(500, "Internal Server Error");
            }
        }
    }
}
