﻿using Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/weatherforcast")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private  ILoggerManager  _logger;
        private readonly IRepositoryManager _repository;

        public WeatherForecastController(ILoggerManager logger, IRepositoryManager repository)
        {
            _logger = logger;
            _repository = repository;

        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            _logger.LogInfo("Here's Info Message from  our values controller");
            _logger.LogDebug("Here's Debug Message from our values controller");
            _logger.LogWarn("Here's Warn Message from our values controller");
            _logger.LogError("Here's Error Message from our values controller");

           /* _repository.Customer.AnyMethodFromRepository();
            _repository.Account.AnyMethodFromAccountRepository();*/

            return new string[] { "value1", "value2" };
        }
    }
}
